<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/28 0028
 * Time: 17:44
 */

namespace Baiyang\Library\Format;


class JsonFormatter implements FormatterInterface
{
    /**
     * Takes the given data and formats it.
     *
     * @param $data
     *
     * @return mixed
     */
    public function format(array $data)
    {
        $options = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
        $options = ENVIRONMENT === 'production' ? $options : $options | JSON_PRETTY_PRINT;
        $result = json_encode($data, $options, 512);
        if (json_last_error() !== JSON_ERROR_NONE)
        {
            throw new \RuntimeException(sprintf("Failed to parse json string, error: '%s'", json_last_error_msg()));
        }
        return $result;
    }
}