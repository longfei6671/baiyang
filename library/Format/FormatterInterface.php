<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/28 0028
 * Time: 17:44
 */

namespace Baiyang\Library\Format;

/**
 * 数据格式化接口
 * @package Baiyang\Library\Format
 */
interface FormatterInterface
{
    /**
     * Takes the given data and formats it.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function format(array $data);
}