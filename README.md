# 百洋项目单模块基础架构

### 项目目录结构

```
Baiyang
|
|-> application
|   |-> Controllers   控制器目录
|   |-> Http        项目内置基类
|   |-> Models 模型目录
|   |-> Services 业务逻辑目录
|   |-> Traits PHP的trait目录
|-> bootstrap 项目初始化文件目录
|-> config  配置目录
|-> library 第三方库以及公共方法
|-> vendor  使用 Composer 控制的第三方库
|-> runtime 缓存，日志以及其他临时文件存在
|-> public  项目入口目录
```

项目配置以功能为维度放入到 `config` 目录,也可以再 `config/app.conf` 中配置，配置信息将会合并。

命名空间注入需要在 `config/namespace.php` 中配置，系统会自动注入。

### 开发规范

- Model 类必须继承自`ModelBase`类，且文件和类名必须以Model结尾
- Service 类必须以 `Service` 结尾，且继承字 `ServiceBase` 类
- 如果使用第三方库，请尽量使用 Composer 工具管理，如果没有 Composer 版，需要将类库放到 `library` 目录
- 所有接口需要以 `Interface` 为后缀

### 已集成的第三方包

- hprose  [hprose php 客户端](https://github.com/hprose/hprose-php)
- chrisboulton/php-resque [基于 redis 的队列库](https://github.com/chrisboulton/php-resque)
- phalcon/devtools [Phalcon 命令工具](https://github.com/phalcon/phalcon-devtools) 
- intervention/image [一个图片处理库](https://github.com/Intervention/image)
- phpoffice/phpexcel [一个Excel处理库](https://github.com/PHPOffice/PHPExcel)
- flysystem [抽象层文件读写库](https://github.com/thephpleague/flysystem)
- PHPExcel [一个Excel处理库](https://github.com/PHPOffice/PHPExcel)
- Requests [一个简化的HTTP请求库](https://github.com/rmccue/Requests)
- random_compat [随机数生成库](https://github.com/paragonie/random_compat)
- php-captcha [一个验证码生成类](https://github.com/lifei6671/php-captcha)
- QR Code [一个二维码生成类库](https://github.com/endroid/QrCode)

### 代码示例

**1、日志记录**

```php
<?php
//Controller中
$this->logger->error('错误信息');
//其他类中,使用静态注入获取对象
$logger = \Phalcon\Di::getDefault()->getShared("logger");
```

通过该方式记录的日志一般位于 `BaiyangDaojia/Runtime/logs` 中。

**2、Redis 使用**

```php
<?php
//Controller中
$this->redis->get('key');
//其他类中,使用静态注入获取对象
$redis = \Phalcon\Di::getDefault()->getShared("redis");
```

**3、缓存使用**

```php
<?php
//Controller中
$this->cache->get('key');
//其他类中,使用静态注入获取对象
$cache = \Phalcon\Di::getDefault()->getShared("cache");
```

**4、密码加密**

```php
<?php
//密码加密使用内置的加密方法
$this->security->hash($password);
//校验密码
$this->security->checkHash($password, $user->password)
```

**5、随机数**

```php
<?php

// ...
$bytes      = $this->random->bytes();

// Generate a random hex string of length $len.
$hex        = $this->random->hex($len);

// Generate a random base64 string of length $len.
$base64     = $this->random->base64($len);

// Generate a random URL-safe base64 string of length $len.
$base64Safe = $this->random->base64Safe($len);

// Generate a UUID (version 4). See https://en.wikipedia.org/wiki/Universally_unique_identifier
$uuid       = $this->random->uuid();

// Generate a random integer between 0 and $n.
$number     = $this->random->number($n);
```

**6、分页**

```php
<?php

use Phalcon\Paginator\Adapter\Model as PaginatorModel;

// Current page to show
// In a controller/component this can be:
// $this->request->getQuery("page", "int"); // GET
// $this->request->getPost("page", "int"); // POST
$currentPage = (int) $_GET["page"];

// The data set to paginate
$robots = Robots::find();

// Create a Model paginator, show 10 rows by page starting from $currentPage
$paginator = new PaginatorModel(
    [
        "data"  => $robots,
        "limit" => 10,
        "page"  => $currentPage,
    ]
);

// Get the paginated results
$page = $paginator->getPaginate();
```

视图中使用：

```html
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Type</th>
    </tr>
    <?php foreach ($page->items as $item) { ?>
    <tr>
        <td><?php echo $item->id; ?></td>
        <td><?php echo $item->name; ?></td>
        <td><?php echo $item->type; ?></td>
    </tr>
    <?php } ?>
</table>
```

**7、文件读写**

文件读写使用的是第三方抽象文件库： [flysystem](https://github.com/thephpleague/flysystem) ； 项目中使用：

```php
<?php
$this->storage->put('1.txt',"aaaa");
$this->storage->get('2017/1.txt');
```

**8、HTTP 请求**

项目中尽量使用第三方库 [Requests](https://github.com/rmccue/Requests) 来进行第三方接口的请求。

```php
<?php

$request = Requests::get('http://httpbin.org/get', array('Accept' => 'application/json'));

$request = Requests::post('http://httpbin.org/post', array(), array('mydata' => 'something'));
```


**9、分布式唯一ID**

分布式ID生成算法使用的是 `Snowflake` , 配置信息在 `config/snowflake` 中：

```php
<?php
$id = $this->snowflake->getNextId();
```

**10、配置文件读取**

Phalcon内置方式读取为：

```php
<?php
$this->config->app->configFile;
```

每一个集合的配置为一个单独的PHP文件，位于 `config/` 目录，文件名称为配置的顶级节点属性。 支持以自定义 `ini` 格式配置文件，该配置文件会覆盖PHP格式的配置信息。

其中 `ini` 中的每个节点与PHP配置的文件名相同。例如： `config/app.php` 中返回的 `baseUri` 配置信息，Phalcon 原生方式读取为：

```php
<?php
$this->config->app->baseUri;
```

如果在 `.env` 中存在 `[app]` 节点下的 `baseUri` 值，则会覆盖掉PHP中的配置。

另外，还可以以函数方式读取配置，例如：

```php
<?php
config('app.baseUri');
```

**11、环境变量读取**

该方法优先从环境变量中读取，如果读取失败，则会从已定义的常量中读取。

```php
<?php

env('APP_PATH');
```






