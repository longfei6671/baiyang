<?php

$router = new \Phalcon\Mvc\Router(false);

$router->removeExtraSlashes(true);
$router->add('/:controller/([a-zA-Z0-9_\-]+)/:params', array(
    'controller' => 1,
    'action'     => 2,
    'params'     => 3,
))->convert('action', function ($action) {
    // transform action from foo-bar -> foo_bar
    $a = str_replace('-', '_', $action);
    // transform action from foo_bar -> fooBar
    return lcfirst(Phalcon\Text::camelize($a));
});
$router->add('/:controller', array(
    'controller' => 1,
));

return $router;