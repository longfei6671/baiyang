<?php
//默认时区配置
date_default_timezone_set('Asia/Shanghai');

define('APP_START', microtime(true));

error_reporting(E_ALL);

require __DIR__ . '/../vendor/autoload.php';

define('BASE_PATH', realpath(dirname(__DIR__) ));

require (BASE_PATH . '/bootstrap/define.php');

try {

    require __DIR__ . '/../library/functions.php';

    //development testing staging production
    define('ENVIRONMENT',env('ENVIRONMENT','development'));
    /**
     * Include Autoloader
     */
    // include (__DIR__ . '/../bootstrap/loader.php');
    $loader = new \Phalcon\Loader();

    /**
     * The FactoryDefault Dependency Injector automatically registers
     * the services that provide a full stack framework.
     */
    $di = require_once (__DIR__ . '/../bootstrap/services.php');


    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    $logger = \Phalcon\Di::getDefault()->getShared('logger');
    $logger->error($e->getMessage()."\r\n".$e->getTraceAsString());
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
