<?php

return array(
    'configFile' => BASE_PATH . '/public/.env',
    'baseUri'       => env('APP_URL','http://localhost'),
    'cacheDir'      => env('CACHE_DIR',RUNTIME_PATH . '/cache/'),
    'viewsDir'      => env('VIEWS_DIR', APP_PATH . '/views/'),
    //视图编译路径
    'compiledPath'  => env('compiledPath', RUNTIME_PATH . '/compiled/'),
    'compiledExtension' =>  env('compiledExtension','.compiled'),
    //一直编译视图
    'viewCompileAlways' => true ,
);