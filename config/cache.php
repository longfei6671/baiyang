<?php

return array(
    'default' => env('CACHE_DRIVER', 'file'),
    "lifetime" => 172800,
    'file' => array(
        'class' => Phalcon\Cache\Backend\File::class,
        "cacheDir" => "../app/cache/"
    ),
    'apc' => array(
        'class' => Phalcon\Cache\Backend\Apc::class,
        "prefix" => "baiyang_",
    ),
    'apcu' => array(
        'class' => Phalcon\Cache\Backend\Apcu::class,
        "prefix" => "baiyang_",
    ),
    'memcached' => array(
        'class' =>  Phalcon\Cache\Backend\Libmemcached::class,
        "servers" => array(
            array(
                "host"   => "localhost",
                "port"   => 11211,
                "weight" => 1,
            ),
        ),
        "client" => array(
            // \Memcached::OPT_HASH       => \Memcached::HASH_MD5,
            // \Memcached::OPT_PREFIX_KEY => "prefix.",
        )
    ),
    'memcache' => array(
        'class' => Phalcon\Cache\Backend\Memcache::class,
        "host"       => "localhost",
        "port"       => 11211,
        "persistent" => false,
    ),
    'memory' => array(
        'class' => Phalcon\Cache\Backend\Memory::class
    ),
    'mongo' => array(
        'class' => Phalcon\Cache\Backend\Mongo::class,
        "server"     => "mongodb://localhost",
        "db"         => "caches",
        "collection" => "images",
    ),
    'redis' => array(
        'class' => Phalcon\Cache\Backend\Redis::class,
        "host"       => "localhost",
        "port"       => 6379,
        "auth"       => "foobared",
        "persistent" => false,
        "index"      => 0,
    ),
    'xcache' => array(
        'class' => Phalcon\Cache\Backend\Xcache::class,
        "prefix" => "baiyang_",
    )
);