<?php
/**
 * 注册命名空间
 */
return array(
    'Baiyang\Controllers' => APP_PATH . '/Controllers/',
    'Baiyang\Library'   => BASE_PATH . '/library/',
    'Baiyang\Traits'    => APP_PATH . '/Traits/',
    'Baiyang\Models'    => APP_PATH . '/Models/',
    'Baiyang\Services'    => APP_PATH . '/Services/',
    'Baiyang\Http'    => APP_PATH . '/Http/',
);