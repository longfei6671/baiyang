<?php
/**
 * driver 是框架内置的Session实现方案，支持的方案有：
 *  Phalcon\Session\Adapter\Files
 *  Phalcon\Session\Adapter\Libmemcached
 *  Phalcon\Session\Adapter\Memcache
 *  Phalcon\Session\Adapter\Redis
 *
 */
return array(
    'driver' => env('SESSION_DRIVER', 'files'),
    'files' => array(
        'class' => Phalcon\Session\Adapter\Files::class,
    ),
    'redis' => array(
        "class" => Phalcon\Session\Adapter\Redis::class,
        "uniqueId"   => "baiyang_",     //前缀
        "host"       => "localhost",    //Host地址
        "port"       => 6379,           //端口号
        "auth"       => "",             //认证信息
        "persistent" => false,          //是否是长连接
        "lifetime"   => 3600,           //默认生命周期
        "prefix"     => "by_",           //Session前缀
        "index"      => 1,              //Redis数据库索引
    ),
    'memcached' => array(
        "class" => Phalcon\Session\Adapter\Libmemcached::class,
         "servers" => [
             [
                 "host"   => "localhost",
                 "port"   => 11211,
                 "weight" => 1,
             ],
         ],
         "client" => [
            // \Memcached::OPT_HASH       => \Memcached::HASH_MD5,
            // \Memcached::OPT_PREFIX_KEY => "prefix.",
         ],
         "lifetime" => 3600,
         "prefix"   => "by_",
    ),
    'memcache' => array(
        "class" => Phalcon\Session\Adapter\Memcache::class,
        "uniqueId"   => "baiyang_",
        "host"       => "127.0.0.1",
        "port"       => 11211,
        "persistent" => true,
        "lifetime"   => 3600,
        "prefix"     => "my_",
    )

);