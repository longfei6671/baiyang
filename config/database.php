<?php

/**
 * 支持Phalcon框架内置的数据库
 */
return array(
    'default' => env('DB_CONNECTION', 'mysql'),
    'mysql' => array(
        'class' => Phalcon\Db\Adapter\Pdo\Mysql::class,
        'host' => env('DB_HOST', '127.0.0.1'),
        'port' => env('DB_PORT', '3306'),
        'dbname' => env('DB_DATABASE', 'forge'),
        'username' => env('DB_USERNAME', 'forge'),
        'password' => env('DB_PASSWORD', ''),
        'charset' => 'utf8mb4',
        "options"  => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES \'UTF8\'",
            PDO::ATTR_CASE               => PDO::CASE_LOWER
        )
    ),
    'sqlite' => array(
        'class' => Phalcon\Db\Adapter\Pdo\Sqlite::class,
        "dbname" => "/tmp/test.sqlite"
    )
);