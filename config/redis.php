<?php
/**
 * redis 配置
 */
return array(
    'default' => array(
        'host' => '192.168.0.226',
        'port' => 6379,
        'auth' => '',
        'persistent' => false,
        'index' => 2,
        'prefix' => 'baiyang_'
    )
);