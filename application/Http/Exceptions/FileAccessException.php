<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/29 0029
 * Time: 9:20
 */

namespace Baiyang\Http\Exceptions;

/**
 * 文件权限访问异常
 * @package Baiyang\Http\Exceptions
 */
class FileAccessException extends IOException
{

}