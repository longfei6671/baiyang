<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/29 0029
 * Time: 9:33
 */

namespace Baiyang\Http\Exceptions;

/**
 * 参数范围异常
 * @package Baiyang\Http\Exceptions
 */
class ArgumentOutOfRangeException extends ArgumentException
{

}