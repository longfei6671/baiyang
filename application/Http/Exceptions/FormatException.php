<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/29 0029
 * Time: 9:31
 */

namespace Baiyang\Http\Exceptions;

/**
 * 格式异常
 * @package Baiyang\Http\Exceptions
 */
class FormatException extends \Phalcon\Exception
{

}