<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/29 0029
 * Time: 9:25
 */

namespace Baiyang\Http\Exceptions;

/**
 * 目录不存在异常
 * @package Baiyang\Http\Exceptions
 */
class DirectionNotFoundException extends  IOException
{

}