<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/29 0029
 * Time: 9:34
 */

namespace Baiyang\Http\Exceptions;

/**
 * 参数空异常
 * @package Baiyang\Http\Exceptions
 */
class ArgumentNullException extends ArgumentException
{

}