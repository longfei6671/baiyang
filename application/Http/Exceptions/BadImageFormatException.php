<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/29 0029
 * Time: 9:33
 */

namespace Baiyang\Http\Exceptions;

/**
 * 图片格式错误异常
 * @package Baiyang\Http\Exceptions
 */
class BadImageFormatException extends \Phalcon\Exception
{

}