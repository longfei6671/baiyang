<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/29 0029
 * Time: 9:27
 */

namespace Baiyang\Http\Exceptions;

/**
 * 路径太长
 * @package Baiyang\Http\Exceptions
 */
class PathTooLongException extends \Phalcon\Exception
{

}