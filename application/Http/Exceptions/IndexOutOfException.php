<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/29 0029
 * Time: 9:27
 */

namespace Baiyang\Http\Exceptions;

/**
 * 索引异常
 * @package Baiyang\Http\Exceptions
 */
class IndexOutOfException extends  \Phalcon\Exception
{

}