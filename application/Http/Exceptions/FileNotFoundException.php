<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/29 0029
 * Time: 9:26
 */

namespace Baiyang\Http\Exceptions;

/**
 * 文件不存在异常
 * @package Baiyang\Http\Exceptions
 */
class FileNotFoundException extends IOException
{

}