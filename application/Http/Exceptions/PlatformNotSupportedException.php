<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/29 0029
 * Time: 9:35
 */

namespace Baiyang\Http\Exceptions;

/**
 * 平台不支持异常
 * @package Baiyang\Http\Exceptions
 */
class PlatformNotSupportedException extends \Phalcon\Exception
{

}