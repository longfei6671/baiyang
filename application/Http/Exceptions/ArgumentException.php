<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/29 0029
 * Time: 9:17
 */

namespace Baiyang\Http\Exceptions;

/**
 * 参数异常
 * @package BaiyangDaojia\Exceptions
 */
class ArgumentException extends \Phalcon\Exception
{
    public function __construct($message = "", $code = 0, \Throwable $previous = null)
    {
        if(empty($message)){
            $message = '无效的参数';
        }
        parent::__construct($message, $code, $previous);
    }
}